package com.k.krakatau;

import py4j.DefaultGatewayServerListener;
import py4j.GatewayServer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

public class Krakatau {

    public static final GatewayServer server;

    private static final Logger LOGGER = Logger.getLogger(Krakatau.class.getName());

    private static Krakatau4J krakatau4J;

    private Krakatau() {
        throw new IllegalAccessError();
    }

    static {
        GatewayServer.turnAllLoggingOn();
        server = new GatewayServer();
        server.addListener(new DefaultGatewayServerListener() {
            @Override
            public void serverStarted() {
                startPythonProcess();
                super.serverStarted();
            }
        });
        Thread daemon = new Thread(() -> server.start(false));
        daemon.setDaemon(true);
        daemon.start();
    }

    private static void startPythonProcess() {
        try {
            Path pythonLib = extractPythonLib();
            Process process = new ProcessBuilder()
                    .command("python", pythonLib.resolve("Krakatau4J").resolve("krakatau4j.py").toString())
                    .inheritIO()
                    .start();
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                LOGGER.info("Destroying python VM process");
                try {
                    process.destroyForcibly().waitFor();
                } catch (InterruptedException e) {
                    LOGGER.log(Level.WARNING, "Cannot shutdown python VM on JVM shutdown", e);
                    Thread.currentThread().interrupt();
                }
            }));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Cannot start python VM", e);
        }
        LOGGER.info("Python VM started successfully with 'py4j' callback");
    }

    private static Path extractPythonLib() throws IOException {
        Path codeSource = Paths.get(Krakatau.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        Path tmp = Files.createTempDirectory("krakatau4j");
        Files.walkFileTree(tmp, new FileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                file.toFile().deleteOnExit();
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                dir.toFile().deleteOnExit();
                return FileVisitResult.CONTINUE;
            }
        });
        tmp.toFile().deleteOnExit();
        if (Files.isDirectory(codeSource)) {
            Path pythonLib = codeSource.resolve("Lib");
            copy(pythonLib, tmp);
        } else {
            try (FileSystem jar = FileSystems.newFileSystem(codeSource, null)) {
                Path pythonLib = jar.getPath("/Lib");
                copy(pythonLib, tmp);
            }
        }
        return tmp;
    }

    private static void copy(Path pythonLib, Path targetDir) throws IOException {
        for (Path path : Files.walk(pythonLib).collect(toList())) {
            Path copy = targetDir.resolve(pythonLib.relativize(path).toString());
            Files.createDirectories(copy.getParent());
            Files.copy(path, copy, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public static void assemble(Collection<File> asmSources, File outDir) {
        waitForPythonProcessGetStarted();
        for (File asmSource : asmSources) {
            krakatau4J.assemble(asmSource.toString(), outDir.toString());
        }
    }

    public static void disassemble(Collection<File> classSources, File outDir) {
        waitForPythonProcessGetStarted();
        for (File classSource : classSources) {
            krakatau4J.disassemble(classSource.toString(), outDir.toString());
        }
    }

    public static void setKrakatau4J(Krakatau4J krakatau4J) {
        LOGGER.info("Setting Krakatau4j python object reference");
        Krakatau.krakatau4J = krakatau4J;
    }

    private static void waitForPythonProcessGetStarted() {
        if (nonNull(krakatau4J)) {
            return;
        }
        Instant start = Instant.now();
        while (isNull(krakatau4J) || ChronoUnit.SECONDS.between(start, Instant.now()) < 5) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "Waiting for python VM was Interrupted", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    private static List<String> getResourceFiles(String path) {
        List<String> fileNames = new ArrayList<>();
        try (InputStream is = getResourceAsStream(path);
             BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String resource;
            while (nonNull(resource = reader.readLine())) {
                fileNames.add(resource);
            }
        } catch (IOException e) {
            throw new RuntimeException("Cannot locate resource files to get python source files", e);
        }
        return fileNames;
    }

    private static InputStream getResourceAsStream(String resource) {
        InputStream is = getContextClassLoader().getResourceAsStream(resource);
        if (isNull(is)) {
            is = Krakatau.class.getResourceAsStream(resource);
            if (isNull(is)) {
                return ClassLoader.getSystemResourceAsStream(resource);
            } else {
                return is;
            }
        } else {
            return is;
        }
    }

    private static ClassLoader getContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

}
