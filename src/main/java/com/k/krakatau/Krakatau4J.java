package com.k.krakatau;

public interface Krakatau4J {

    void assemble(String asmSource, String outDir);
    void disassemble(String classSource, String outDir);

}
