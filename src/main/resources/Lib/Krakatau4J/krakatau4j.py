import os
import sys

lib_path = os.path.abspath(os.path.join(__file__, '..', '..'))
sys.path.append(lib_path)

import socket, sched, time

from py4j.java_gateway import (JavaGateway, DEFAULT_PORT as JAVA_GATEWAY_DEFAULT_PORT)
import disassemble, assemble


class Krakatau4JImpl:

    def disassemble(self, classSource, outDir):
        try:
            out = disassemble.script_util.makeWriter(outDir, '.j')
            disassemble.disassembleSub(disassemble.readFile, out, [classSource])
        except:
            pass

    def assemble(self, asmSource, outDir):
        try:
            out = disassemble.script_util.makeWriter(outDir, '.class')
            pairs = assemble.assembleClass(asmSource)
            for name, data in pairs:
                out.write(name, data)
        except:
            pass

    class Java:
        implements = ["com.k.krakatau.Krakatau4J"]



def shutdown_on_java_gateway_death():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            s.connect(("127.0.0.1", JAVA_GATEWAY_DEFAULT_PORT))
            sys.exit()
        except socket.error as e:
            if e.errno == 98:
                print("Port is already in use")
            else:
                # something else raised the socket.error exception
                print(e)


if __name__ == '__main__':
    gateway = JavaGateway(start_callback_server=True)
    krakatau = Krakatau4JImpl()
    gateway.jvm.com.k.krakatau.Krakatau.setKrakatau4J(krakatau)

    s = sched.scheduler(time.time, time.sleep)
    s.enter(5, 1, shutdown_on_java_gateway_death, ())
